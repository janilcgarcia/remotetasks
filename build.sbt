ThisBuild / scalaVersion := "3.1.0"

ThisBuild / organization := "br.dev.safelydysfunctional"

lazy val root = project.in(file("."))
  .settings(
    name := "remote-tasks",
    version := "0.1.0-SNAPSHOT",
    libraryDependencies += "com.rabbitmq" % "amqp-client" % "5.14.0",
    libraryDependencies += "org.apache.commons" % "commons-pool2" % "2.11.1",

    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser"
    ).map(_ % "0.14.1")
  )
