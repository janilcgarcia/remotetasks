package remotetasks

import com.rabbitmq.client.Connection

import Extensions._
import scala.concurrent.Promise
import scala.concurrent.Future

import scala.concurrent.ExecutionContext.Implicits.global
import java.util.UUID
import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Consumer
import com.rabbitmq.client.ShutdownSignalException
import com.rabbitmq.client.Envelope
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Channel
import com.rabbitmq.client.ShutdownListener
import org.apache.commons.pool2.PooledObject
import scala.util.Either
import io.circe._
import io.circe.parser.decode
import io.circe.generic.semiauto._
import io.circe.disjunctionCodecs.decoderEither

case class TaskPromise(
  val promise: Promise[Array[Byte]],
  val channel: Channel,
  val taskId: String
)

case class TaskExecRequest(
  val taskName: String,
  val reply: Boolean,
  val arguments: Json
)

object TaskExecRequest {
  given Codec[TaskExecRequest] = deriveCodec
}

class TaskDispatcher(
  queue: String, connection: Connection
) {
  private val replyChannel = connection.createChannel()
  private val replyQueue = replyChannel.queueDeclare().getQueue()

  private val replyTag = replyChannel.basicConsume(
    replyQueue,
    true,
    consumer
  )

  private val channelPool = ChannelPool(connection)

  private val resolvingFutures =
    collection.mutable.Map.empty[String, TaskPromise]

  def fireForResult[A, R](
    task: RemoteTask[_, A, R]
  )(a: A)(implicit E: Encoder[A], R: Decoder[R]): Future[R] = {
    val message = TaskExecRequest(task.name, true, E(a))

    val payload = summon[Encoder[TaskExecRequest]](message).noSpaces

    val promise = Promise[Array[Byte]]()
    val taskId = UUID.randomUUID().toString

    channelPool.withObject { channel =>
      val props = new AMQP.BasicProperties.Builder()
        .correlationId(taskId)
        .replyTo(replyQueue)
        .build()

      val shutdownListener: ShutdownListener = { exc =>
        promise.tryFailure(exc)
      }

      promise.future.onComplete(_ =>
        channel.removeShutdownListener(shutdownListener))

      channel.basicPublish("", queue, props, payload.getBytes)
      resolvingFutures.synchronized {
        resolvingFutures += taskId -> TaskPromise(promise, channel, taskId)
      }
    }

    promise.future.map { bytes =>
      val s = new String(bytes)
      val result = decode[Either[String, R]](s).getOrElse(
        throw new Exception("Can't parse response"))

      result match {
        case Left(s) => throw new Exception(s)
        case Right(value) => value
      }
    }
  }

  def fireAndForget(n: Int): Unit = {
    channelPool.withObject { channel =>
      channel.basicPublish("", queue, null, n.toString.getBytes)
    }
  }

  def fireForResult(n: Int): Future[Int] = {
    val promise = Promise[Array[Byte]]()
    val taskId = UUID.randomUUID().toString()

    channelPool.withObject { channel =>
      val props = new AMQP.BasicProperties.Builder()
        .correlationId(taskId)
        .replyTo(replyQueue)
        .build()

      val shutdownListener: ShutdownListener = exc => {
        promise.tryFailure(exc)
      }
      promise.future.onComplete { _ =>
        channel.removeShutdownListener(shutdownListener)
      }

      channel.addShutdownListener(shutdownListener)
      channel.basicPublish("", queue, props, n.toString.getBytes())

      resolvingFutures.synchronized {
        resolvingFutures += taskId -> TaskPromise(promise, channel, taskId)
      }
    }

    promise.future.map(new String(_).toInt)
  }

  private object consumer extends Consumer {
    def handleConsumeOk(tag: String): Unit = {}
    def handleCancelOk(tag: String): Unit = {}

    def handleCancel(tag: String): Unit = {}

    def handleShutdownSignal(
      tag: String, exc: ShutdownSignalException): Unit = {}

    def handleRecoverOk(tag: String): Unit = {}

    def handleDelivery(
      tag: String, envelope: Envelope,
      props: AMQP.BasicProperties, body: Array[Byte]
    ): Unit = {
      if (resolvingFutures.contains(props.getCorrelationId())) {
        resolvingFutures.synchronized {
          resolvingFutures(props.getCorrelationId()).promise.trySuccess(body)
          resolvingFutures -= tag
        }
      }
    }
  }
}

object TaskDispatcher {
  def apply(
    queue: String,
    host: String = "localhost",
    port: Int = 5672
  ): TaskDispatcher = new TaskDispatcher(
    queue,

    {
      val factory = new ConnectionFactory
      factory.setHost(host)
      factory.setPort(port)
      factory.newConnection()
    }
  )
}
