package remotetasks

import io.circe.Encoder
import io.circe.Decoder
import io.circe.Json
import io.circe.parser.decode

trait ReadyRemoteTask[A, R] {
  protected given argumentDecoder: Decoder[A]
  protected given resultEncoder: Encoder[R]

  def name: String

  def apply(a: A): R

  def execute(args: Json): Unit =
    executeForResult(args)

  def executeForResult(args: Json): Json = {
    val a = try {
      args.as[A].getOrElse(
        throw new IllegalArgumentException("Can't parse arguments"))
    } catch {
      case ex: Throwable =>
        println(ex)
        throw ex
    }

    val r = this(a)

    resultEncoder(r)
  }
}

trait RemoteTask[C, A, R] {
  def setup(context: C): ReadyRemoteTask[A, R]
  def name: String
}

object RemoteTask {
  def pure[A, R](taskName: String)(
    f: A => R
  )(implicit A: Decoder[A], R: Encoder[R]): RemoteTask[Unit, A, R] =
    new RemoteTask { self =>
      def name: String = taskName

      def setup(c: Unit): ReadyRemoteTask[A, R] = new ReadyRemoteTask {
        def name = self.name
        def apply(a: A): R = f(a)

        given argumentDecoder: Decoder[A] = A
        given resultEncoder: Encoder[R] = R
      }
    }
}
