package remotetasks

import org.apache.commons.pool2.ObjectPool

class ObjectPoolOps[A](val pool: ObjectPool[A]) extends AnyVal {
  def withObject[B](f: A => B): B = {
    val ref = pool.borrowObject()
    try {
      f(ref)
    } finally {
      pool.returnObject(ref)
    }
  }
}
