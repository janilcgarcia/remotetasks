package remotetasks

import com.rabbitmq.client.Connection
import com.rabbitmq.client.Channel
import scala.util.Using
import com.rabbitmq.client.Consumer
import com.rabbitmq.client.ShutdownSignalException
import com.rabbitmq.client.Envelope
import com.rabbitmq.client.AMQP
import scala.concurrent.Future

import scala.concurrent.ExecutionContext.Implicits.global
import org.apache.commons.pool2.impl.GenericObjectPool
import org.apache.commons.pool2.BasePooledObjectFactory
import org.apache.commons.pool2.PooledObject
import org.apache.commons.pool2.PooledObjectFactory
import org.apache.commons.pool2.impl.DefaultPooledObject

import Extensions._
import com.rabbitmq.client.ConnectionFactory
import scala.io.StdIn
import scala.annotation.tailrec
import com.rabbitmq.client.AlreadyClosedException
import io.circe._
import io.circe.parser.decode
import io.circe.disjunctionCodecs.encodeEither
import scala.concurrent.ExecutionContext
import java.util.concurrent.Executors

class Worker(
  queue: String,
  connection: Connection,

  private val tasks: Map[String, ReadyRemoteTask[_, _]]
) {
  private val channel = connection.createChannel()
  private val replyChannelPool = ChannelPool(connection)

  channel.queueDeclare(queue, false, false, false, null)

  private val ctag = channel.basicConsume(queue, false, consumer)

  def close(): Unit = {
    try {
      Using(connection) { connection =>
        if (connection.isOpen()) {
          val ch = if (channel.isOpen())
            channel
          else
            connection.createChannel()

          Using(ch) { channel =>
            channel.basicCancel(ctag)
          }
        }
      }
    } catch {
      case _: AlreadyClosedException =>
    }
  }

  private object consumer extends Consumer {
    def handleConsumeOk(tag: String): Unit = {}

    def handleShutdownSignal(tag: String, ex: ShutdownSignalException)
        : Unit = {}

    def handleRecoverOk(tag: String): Unit = {}

    def handleDelivery(
      tag: String, envelope: Envelope,
      props: AMQP.BasicProperties, body: Array[Byte]
    ): Unit = {
      val deliveryId = envelope.getDeliveryTag()
      val task = decode[TaskExecRequest](new String(body))
        .getOrElse(
          throw new Exception("Invalid request"))

      println(s"task[$deliveryId] - ${task.taskName}(${task.arguments})")

      val replyQueue = Option(props.getReplyTo())
      val correlationId = Option(props.getCorrelationId())

      given ExecutionContext = ExecutionContext.fromExecutor(
        Executors.newFixedThreadPool(6)
      )

      Future {
        val result = try {
          tasks.get(task.taskName).fold(
            Left(s"Task ${task.taskName} is not registred in the worker")
          )(
            t => Right(t.executeForResult(task.arguments))
          )
        } catch {
          case ex: Throwable =>
            ex.printStackTrace(System.err)
            throw ex
        }

        println(s"task[$deliveryId] - result = $result")

        for {
          queue <- replyQueue
          id <- correlationId
        } {
          println(s"task[$deliveryId] - replying to $queue")
          replyChannelPool.withObject { channel =>
            channel.basicPublish(
              "", queue,
              new AMQP.BasicProperties.Builder()
                .correlationId(id)
                .build(),

              summon[Encoder[Either[String, Json]]](result).noSpaces.getBytes
            )
          }
        }

        channel.basicAck(envelope.getDeliveryTag(), false)
        println(s"task[$deliveryId] - done")
      }
    }

    def handleCancel(ctag: String): Unit = {}

    def handleCancelOk(ctag: String): Unit = {}
  }

  @tailrec
  private final def fib(n: Int, a: Int = 1, b: Int = 1): Int =
    if (n > 0)
      fib(n - 1, b, a + b)
    else
      a
}

object Worker {
  def apply(
    queue: String,
    host: String = "localhost",
    port: Int = 5672
  ): Worker =
    new Worker(
      queue,
      {
        val factory = new ConnectionFactory()
        factory.setHost(host)
        factory.setPort(port)

        factory.newConnection()
      },
      Map(
        ExampleTasks.fib.name ->
          ExampleTasks.fib.setup(())
      )
    )

  def main(args: Array[String]): Unit = {
    val worker = Worker("rpc_queue")

    println("Worker receiving tasks, press Enter to quit")
    StdIn.readLine()

    worker.close()
  }
}
