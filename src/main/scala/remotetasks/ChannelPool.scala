package remotetasks

import org.apache.commons.pool2.ObjectPool
import org.apache.commons.pool2.PooledObjectFactory
import org.apache.commons.pool2.impl.GenericObjectPool
import com.rabbitmq.client.Channel
import org.apache.commons.pool2.PooledObject
import com.rabbitmq.client.Connection
import org.apache.commons.pool2.impl.DefaultPooledObject
import scala.util.Try
import org.apache.commons.pool2.impl.GenericObjectPoolConfig

object ChannelPool {
  class ChannelFactory(connection: Connection)
      extends PooledObjectFactory[Channel] {

    def makeObject(): PooledObject[Channel] =
      new DefaultPooledObject(connection.createChannel())

    def destroyObject(p: PooledObject[Channel]): Unit = {
      val channel = p.getObject()

      if (channel.isOpen())
        Try(channel.close())
    }

    def validateObject(p: PooledObject[Channel]): Boolean = {
      p.getObject().isOpen()
    }

    def activateObject(p: PooledObject[Channel]): Unit = {}
    def passivateObject(p: PooledObject[Channel]): Unit = {}
  }

  private def defaultConfig = {
    val config = new GenericObjectPoolConfig[Channel]
    config.setTestOnBorrow(true)
    config
  }

  def apply(connection: Connection): ObjectPool[Channel] =
    ChannelPool(new ChannelFactory(connection))

  def apply(factory: ChannelFactory): ObjectPool[Channel] =
    new GenericObjectPool(factory, defaultConfig)
}
