package remotetasks

import scala.annotation.tailrec

object ExampleTasks {
  val fib = RemoteTask.pure[Int, BigInt]("tasks.fibonacci") { n =>
    @tailrec
    def go(n: Int, a: BigInt, b: BigInt): BigInt = {
      if (n > 0)
        go(n - 1, b, a + b)
      else
        a
    }

    go(n, 1, 1)
  }
}
