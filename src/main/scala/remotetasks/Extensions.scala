package remotetasks

import scala.language.implicitConversions

import org.apache.commons.pool2.ObjectPool

object Extensions {
  implicit def toObjectPoolOps[A](pool: ObjectPool[A]): ObjectPoolOps[A] =
    new ObjectPoolOps[A](pool)
}
